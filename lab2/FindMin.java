public class FindMin {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);
        int mincondition = a < b ? a : b ;
        mincondition = mincondition < c ? mincondition : c; 
        System.out.println("minimun = " + mincondition);
    }
}