public class FindPrimes {
    public static void main (String[] args){
        System.out.println(args[0]);
        int max = Integer.parseInt(args[0]);
        for(int number = 2; number< max ;number++){
            int divisor = 2 ;
            boolean isprime = true;
            while (divisor < number && isprime) {
                if (number % divisor == 0) {
                    isprime = false;
                }
                divisor++;
            }
            if (isprime)
                System.out.print(number + ",");
    }
    }
}
